## Title:Kyushu blog backend
L'objectif du projet est de faire en groupe la conception du modèle, créer une API Rest avec Symfony (les contrôleurs et la validation), et mettre en place la base de données et les composants d'accès aux données pour une application de blog.

## introduction
ce projet de blog backend contenant:
-les différentes classes entités et repositories:
-Les fichiers SQL contenant la création des tables, l'insertion du jeu de données, et les requêtes pour certaines des fonctionnalités

## functionalités principals:usecase
![usecase-projetblog](/uploads/b72caf2944a42aabf3181e1322b47376/usecase-projetblog.png)

## functionalités principals:diagrame de classes
![Capture_d_écran_2023-02-22_à_05.14.50](/uploads/bc36f660b693c04a318593d5c9b44d7b/Capture_d_écran_2023-02-22_à_05.14.50.png)

## auteur
Hisami Stolz
