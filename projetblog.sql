DROP TABLE IF EXISTS region;
DROP TABLE IF EXISTS category;

DROP TABLE IF EXISTS article;


DROP TABLE IF EXISTS comment;

CREATE TABLE region (
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    name VARCHAR(100),
    rimg VARCHAR(350),
    color VARCHAR(30),
    description VARCHAR(1000)
);

INSERT INTO region (name,rimg,color,description) VALUES 
("Fukuoka",'https://3.bp.blogspot.com/-eXEEYQ99--0/WMJLH7dKggI/AAAAAAABCds/WXIKSaXlN74zgX0Hl4-_60bIN8cLzUqPwCLcB/s180-c/japan_character8_kyuusyuu1_fukuoka.png','red',"Fukuoka est une des villes les plus dynamiques culturellement du Japon, avec des pratiques culturelles typiques et une situation de carrefour historique entre l'archipel japonais et le continent asiatique."),
("Saga","https://4.bp.blogspot.com/-5EIZzwi3OgY/WMJLIJHFMxI/AAAAAAABCdw/D2RB4I08-40nhe3_z6VA1fVIY-HfcqBAQCLcB/s400/japan_character8_kyuusyuu2_saga.png","green","La région de Saga a connu très tôt la riziculture et des vestiges de rizières de cette époque ont été retrouvées aux ruines de Nabatakeiseki à Kuratsu et sur le site de Yoshinogari."),
("Nagasaki","https://4.bp.blogspot.com/-LcAGSIbs4HQ/WMJLITLCV2I/AAAAAAABCd0/u8soug6FO4EBY64lwCW9wn3Wz-XWAT84wCLcB/s180-c/japan_character8_kyuusyuu3_nagasaki.png","blue","L'histoire de Nagasaki a presque entièrement été construite par des étrangers ; en effet, ce sont les Portugais qui en font une ville portuaire prospère au xvie siècle."),
("Kumamoto","https://3.bp.blogspot.com/--sbT90kvW30/WMJLIms8UlI/AAAAAAABCd4/yGKPQIQV-4I1b-ghpHiR8opBz5OoVFK6gCLcB/s180-c/japan_character8_kyuusyuu4_kumamoto.png","yellow","Dans la préfecture, le tourisme se développe de plus en plus, notamment grâce aux monuments historiques tels que le château de Kumamoto ou encore le pont Tsujunkyo."),
("Oita","https://1.bp.blogspot.com/-uEqcqdspoIE/WMJLJPr0C5I/AAAAAAABCd8/mXaaXFIAv8MD9qWwsQ1oR196-DIbWL2QwCLcB/s180-c/japan_character8_kyuusyuu5_ooita.png","orange","la préfecture d'Oita est un endroit où se ressourcer et entrer en contact avec la nature comme les villes onsen "),
("Miyazaki","https://4.bp.blogspot.com/-LouPx31vSRc/WMJLJJr-tnI/AAAAAAABCeA/khf45DMVuc0W_NjHj23q4YiQffMzmZjuwCLcB/s180-c/japan_character8_kyuusyuu6_miyazaki.png","skyblue","La préfecture de Miyazaki se trouve sur l'île de Kyūshū, au sud du Japon. Elle est réputée pour ses plages et ses routes au bord de l'océan."),
("Kagoshima","https://2.bp.blogspot.com/-V87hwmePDJc/WMJLJopPxSI/AAAAAAABCeE/YwTg32S52nYZeYKAMnRtW1jlk3DafNwWACLcB/s180-c/japan_character8_kyuusyuu7_kagoshima.png","violet","La région est connue pour son climat subtropical, ses sources chaudes, ses volcans et ses parcs nationaux.");



CREATE TABLE category (
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    name VARCHAR(100)
);

INSERT INTO category (name) VALUES 
("Visit"),
("Gastronomie"),
("Évenement"),
("Artisanat");


CREATE TABLE article (
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    title VARCHAR(100),
    text  VARCHAR(10000),
    user VARCHAR(100),
    favorite BOOLEAN,
    id_region INT,
    id_category INT,
    Foreign Key (id_region) REFERENCES region(id) ON DELETE CASCADE,
    Foreign Key (id_category) REFERENCES category(id) ON DELETE CASCADE,
    likecount INT,
    date DATETIME,
    img VARCHAR(350)
);

INSERT INTO article(title,text,user,favorite,id_region,id_category,likecount,date,img) VALUES 
("Visit Karatsu Castle","Le château de Karatsu (唐津城, Karatsu-jō?) est un château situé dans la ville japonaise de Karatsu, dans la préfecture de Saga.Terazawa Hirotaka, un vassal de Hideyoshi Toyotomi, obtient le domaine de Karatsu en 1595. Lors de la bataille de Sekigahara en 1600, il se joint à Ieyasu Tokugawa et attaque le château de Gifu. Il gagne en remerciement des territoires aux alentours de Karatsu et ordonne la construction du château pour protéger ses nouvelles terres. La construction dure six ans, de 1602 à 1608. On utilise des matériaux issus du château de Nagoya, appartenant à Hideyoshi Toyotomi, qui est démantelé.Le château est situé sur une colline dénommée mantōzan (満島山?) et, lors de sa construction, il était au centre d'une forêt de pins s'étendant de part et d'autre le long de la mer. Cette forêt rappelait une grue étendant ses ailes, donnant au château son surnom « château de la grue dansante » (舞鶴城, maizurujō?). Cette forêt a aujourd'hui pratiquement disparu de la rive gauche de la rivière Matsuura, tandis que subsiste la forêt Nijinomatsubara sur la rive droite.","Hugo",false,2,1,0,'2023-02-01',"https://3.bp.blogspot.com/-OdiYyXAsoVk/XGjzBNI7I8I/AAAAAAABRlM/NWH4CeH04mEmJmGSv2togXn5MX_h2YulwCLcBGAs/s500/omatsuri_karatsu_kunchi.png"),("Delicious ramen","De fines nouilles, une consistance crémeuse, un savoureux goût de porc... Pas de doute, il s’agit du célèbre bouillon de Fukuoka.
C’est la fierté de Fukuoka, les Kyushu râmen, autrement appelés Hakata râmen (de l’ancien nom de la capitale du Kyushu, Hakata), dont on dit ici que ce sont tout simplement les plus goûtues du pays ! Leur particularité : un bouillon aux os de porc ! Cette préparation appelée tonkotsu est réputée dans tout le pays.Pour certains, la ville serait même le berceau des râmen japonais. Sa proximité avec d'autres pays d'Asie aurait permis l'introduction sur le sol nippon des célèbres nouilles chinoises, via la porte d'entrée des influences étrangères que constitue Fukuoka depuis des siècles. Mais d’autre puristes soulignent que c’est à Kurume - une petite ville au sud de Fukuoka - qu’a ouvert le premier restaurant de râmen de la région...Plat extrêmement populaire au Japon, les râmen sont déclinées en dizaines de recettes locales. Dans les autres villes du Kyushu, on savoure ces tonkotsu râmen en y ajoutant plus ou moins de viande et de sauce soja (Kumamoto, Kagoshima). Et en dehors, les deux grandes recettes rivales de Fukuoka sont celles de Sapporo (et ses râmen à la soupe miso) et de Tokyo (shio râmen, assaisonnées de sel).","Joan",false,1,2,0,'2023-02-02',"https://4.bp.blogspot.com/-03kgxSH1XOs/UrlnIiMrcGI/AAAAAAAAcOc/gtxPOYEzMAE/s400/ramen_tonkotsu.png"),("Go to Nagasaki Kunchi","Le Kunchi (くんち?), aussi Nagasaki kunchi (長崎くんち?) ou Nagasaki okunchi (長崎おくんち?), est la fête la plus connue de Nagasaki au Japon. Elle commence comme une célébration de la récolte d'automne à la fin du xvie siècle et devient un festival de sanctuaire lorsque le Suwa-jinja est fondé en 1642. Un autre objectif est de découvrir les chrétiens cachés après l'interdiction du christianisme. Cela se manifeste encore aujourd'hui dans la coutume de « montrer le jardin » (庭見せ, niwamise?), lorsque les quartiers représentés ouvrent leurs maisons à l'examen public.Un des spectacles les plus célèbres du festival est la danse du dragon qui est à l'origine représentée au réveillon de la Saint-Sylvestre par les résidents chinois de Nagasaki. Les répétitions pour le festival débutent le 1er juin. Du 7 au 9 octobre, les présentations qui reflètent vivement l'histoire colorée de Nagasaki débordent sur les trois sites du festival dans les rues et créent une atmosphère de fête dans toute la ville.","Samuel",false,3,3,0,'2023-02-03',"https://1.bp.blogspot.com/-_kjXGEfkRAk/VsGsJmSi3JI/AAAAAAAA38k/5jteIsHjdQk/s400/nagasaki_rantan_lantern.png"),("Go to baloon festa","An array of colorful balloons, both large and small, decorates the Kasegawa riverbed in the city of Saga, in Saga Prefecture—it must be the International Balloon Fiesta! First launched in Kyushu in 1978, it is one of the largest such festivals in Asia. The number of balloons participating is sometimes well over 100, and the festival receives entrants from the United States, the United Kingdom, Switzerland, Belgium, South Korea, and dozens of other countries too. For Saga locals, this high-flying event has the poetic charm of fall. People often head out in a picnic mood to watch the sight of the balloons floating in the skies.","Mami",true,2,1,0,'2023-02-01',"https://3.bp.blogspot.com/-OdiYyXAsoVk/XGjzBNI7I8I/AAAAAAABRlM/NWH4CeH04mEmJmGSv2togXn5MX_h2YulwCLcBGAs/s500/omatsuri_karatsu_kunchi.png"),("Hiking Aso Mountain","Il est situé dans la préfecture de Kumamoto, au centre de l'île de Kyūshū et culmine à 1 592 mètres d'altitude. Il n'est qu'à 75 kilomètres à l’est du mont Unzen et à 150 kilomètres au nord du volcan Sakurajima.Ce complexe volcanique regroupe en fait une quinzaine de cônes volcaniques au sein d'une caldeira de 25 sur 18 kilomètres. Cette dernière contient entre autres la ville d'Aso ; on estime que la population qui habite dans cette zone s'élève à 100 000 personnes2.
Parmi tous les cônes volcaniques, les plus grands sont le Naka-dake (1 506 mètres, le plus actif dans les temps historiques), le Taka-dake (le plus élevé, qui culmine à 1 592 mètres), le Neko-dake (1 408 mètres, estimé le plus ancien), le Kijima-dake (1 270 mètres), le Narao-dake (1 331 mètres) et le Eboshi-dake (1 337 mètres).","Taro",false,4,1,0,'2023-02-01',"http://3.bp.blogspot.com/-C0RSkjRoVD0/VhHgqh1r39I/AAAAAAAAzBo/uRrvZLGrBDk/s180-c/yuuyake_yama.png"),("Monkey mountain","Il est situé dans la préfecture de Kumamoto, au centre de l'île de Kyūshū et culmine à 1 592 mètres d'altitude. Il n'est qu'à 75 kilomètres à l’est du mont Unzen et à 150 kilomètres au nord du volcan Sakurajima.
Ce complexe volcanique regroupe en fait une quinzaine de cônes volcaniques au sein d'une caldeira de 25 sur 18 kilomètres. Cette dernière contient entre autres la ville d'Aso ; on estime que la population qui habite dans cette zone s'élève à 100 000 personnes2.Parmi tous les cônes volcaniques, les plus grands sont le Naka-dake (1 506 mètres, le plus actif dans les temps historiques), le Taka-dake (le plus élevé, qui culmine à 1 592 mètres), le Neko-dake (1 408 mètres, estimé le plus ancien), le Kijima-dake (1 270 mètres), le Narao-dake (1 331 mètres) et le Eboshi-dake (1 337 mètres).","Hanako",false,5,1,0,'2023-02-01',"http://1.bp.blogspot.com/-LpxmQyro9hI/VXOT9cyMjTI/AAAAAAAAuFk/ffOMEfkt_kg/s180-c/saru_saruyama_boss.png"),("Kagoshima porc","Dès que vous vous baladerez dans la ville ou que vous mettrez la main sur un guide touristique, vous remarquerez la mention du kurobuta, qui signifie porc noir. Pourquoi est-ce appelé ainsi ? Et quel goût cela a-t-il ? Telles étaient les questions que je me posais et auxquelles j'ai trouvé les réponses chez Ichiniisan.En entrant dans la boutique située à Tenmonkan, vous passerez devant un grand frigidaire et pourrez voir de nombreuses assiettes de fines tranches de porc. Cette viande n'est évidemment pas noire; elle a l'habituelle couleur rose pâle du porc. La réponse à ma première interrogation est simple les kurobuta sont des cochons à la peau noire ! Maintenant, vous pensez peut-être oui, et alors ? Et bien, la première fois que j'ai entendu parler de kurobuta, j'ai pensé qu'il s'agissait peut-être d'une sorte de viande dans une sauce de couleur foncée. Un mystère était donc résolu, pour moi au moins. Il était temps de découvrir le goût du kurobuta","Lilly",false,7,2,0,'2023-02-01',"http://4.bp.blogspot.com/-O1h4L5OvP3U/VWmAawuYD6I/AAAAAAAAtwg/H6lorRZqmjg/s180-c/animal_kurobuta_pig.png"),("Miyazaki Hyottoko Festival","Si vous vous trouvez dans la région de Hyuga , au nord de Miyazaki, pendant le premier samedi soir d'août, alors vous passerez un moment formidable. Le festival d'été de hyottoko de Hyuga est l'un des plus importants festivals Hyottoko dansants d'été du Japon, attirant plus de 2000 danseurs et 70 000 visiteurs.L'événement a lieu dans la ville de Hyuga , au nord de la préfecture de Miyazaki, et on peut y accéder en train ou en voiture.Pour venir en train, prenez la ligne principale JR Nippo depuis Miyazaki et prenez la direction de Nobeoka. Descendez à la gare de Hyugashi au bout de 45 minutes. Le festival se tient aux alentours de la gare.Pour venir en train, prenez la ligne principale JR Nippo depuis la gare de Miyazaki et prenez la direction de Nobeoka. Cherchez les panneaux de signalisation indiquant la gare de Hyuga sur la gauche. Le trajet dure environ 1 h 30 selon la circulation.","George",false,6,2,0,'2023-02-01',"https://2.bp.blogspot.com/-RGf3v3X-Jm4/XDXb1Mo-JXI/AAAAAAABREk/cfV55uuHvqUpnI2j2dxDQ0L616hhGV20wCLcBGAs/s180-c/dance_hyottoko_odori.png"),("Hakata Dontaku Festival","Chaque année, ce festival qui est un des plus grands du Japon se déroule pendant un long week-end de mai à Fukuoka , et attire 30 000 participants et plus de deux millions de spectateurs. Bien qu'il y ait eu des interruptions dans le passé, le festival aurait vu le jour en 1179 pendant l'ère Heian, et s'est maintenu durant plus de 800 ans. Le festival se déroule à proximité de la gare de Tenjin qui se trouve sur la ligne de métro de la gare de Hakata. Les événements de Dontaku se déroulent dans toute la ville de Fukuoka , mais l'itinéraire principal du défilé part de la gare de Gofukumachi jusqu'au parc central de Tenjin. Un défilé plus petit a lieu juste à l'extérieur de la gare de Hakata. Récemment, il y a eu des événements autour du quartier du port de Hakata.","George",false,1,3,0,'2023-02-01',"https://2.bp.blogspot.com/-RGf3v3X-Jm4/XDXb1Mo-JXI/AAAAAAABREk/cfV55uuHvqUpnI2j2dxDQ0L616hhGV20wCLcBGAs/s180-c/dance_hyottoko_odori.png"),("La porcelaine d’Arita","Fabriquée dans la préfecture de Saga dès la fin du XVIe siècle après la découverte d’une argile appropriée à sa conception dans la région d’Arita, cette porcelaine s’est très vite développée en une variété de styles qui furent commercialisés et exportés, notamment pour une clientèle nationale,  mais également européenne. Ainsi, en fonction de la période de la création de l’objet, vous pourrez retrouver des motifs aux couleurs et aux détails proches de la porcelaine chinoise, rehaussés par de l’or, comme une vaisselle à l’émail et aux décors plus sobres.","Helene",false,2,4,0,'2023-02-01',"http://2.bp.blogspot.com/-KG-Idox_f70/VlAZAjOHjvI/AAAAAAAA03g/nQKqJxRNXvc/s180-c/imariyaki.png"),("Hakata gion Yamagasa","Ses rites se déroulent au Kushida-jinja. Célèbre pour sa course de chars d'une tonne, son histoire est longue de quelque sept cent cinquante ans. Il attire jusqu'à un million de spectateurs et, en 1979, a été désigné comme « importante propriété culturelle folklorique immatérielle1,2,3 ». Il a aussi été inscrit en 2016 sur la Liste représentative du patrimoine culturel immatériel de l’humanité de l’UNESCO4","Helene",false,2,4,0,'2023-02-01',"https://4.bp.blogspot.com/-wyYPSBDgu_c/V6iIKcId5WI/AAAAAAAA8_s/EpaQMYMKAfMZ8697932E1-dqQcZHae-SwCLcB/s180-c/omikoshi_hakata_gion_yamakasa.png")
;


CREATE TABLE comment (
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    cuser VARCHAR(100),
    ctext VARCHAR(500),
    id_article INT,
    Foreign Key (id_article) REFERENCES article(id)ON DELETE CASCADE
);

INSERT INTO comment (cuser,ctext,id_article) VALUES 
("User1","hello",1);

SELECT * FROM region;
SELECT * FROM category;
SELECT * FROM article ;

SELECT * FROM comment ;

INSERT INTO article (title,text,user,favorite,id_region,id_category,likecount,date,img) VALUES ("Hiking at Aso","lorem","Hana",true,4,1,0,"2023-01-02","img4");

SELECT * ,article.id a_id,region.id r_id,region.name r_n,category.id c_id,category.name c_n FROM article LEFT JOIN region ON article.id_region=region.id LEFT JOIN category ON article.id_category=category.id WHERE region.id=2;

UPDATE article SET title="",text="",user="",favorite=true,id_region=7,id_category=1,likecount=0,date="2021-01-23",img="iuagf" WHERE id=15;

SELECT * ,article.id a_id,region.id r_id,region.name r_n,category.id c_id,category.name c_n FROM article LEFT JOIN region ON article.id_region=region.id LEFT JOIN category ON article.id_category=category.id WHERE favorite=1