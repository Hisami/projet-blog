<?php

namespace App\Entities;
use App\Entities\Region;
use App\Entities\Category;
use DateTime;

class Article{
    private ?int $id;
    private ?string $title;
    private ?string $text;
    private ?string $user;
    private ?bool $favorite;
    private ?Region $region;
    private ?Category $category;
    private ?int $likecount;
    private ?DateTime $date;
	private ?string $img;


	/**
	 * @param int|null $id
	 * @param string|null $title
	 * @param string|null $text
	 * @param string|null $user
	 * @param bool|null $favorite
	 * @param int|null $likecount
	 * @param DateTime|null $date
	 * @param string|null $img
	 */
	public function __construct(?string $title, ?string $text, ?string $user, ?bool $favorite, ?int $likecount, ?DateTime $date, ?string $img,?int $id) {
		$this->title = $title;
		$this->text = $text;
		$this->user = $user;
		$this->favorite = $favorite;
		$this->likecount = $likecount;
		$this->date = $date;
		$this->img = $img;
		$this->id = $id;
	}





	/**
	 * @return int|null
	 */
	public function getId(): ?int {
		return $this->id;
	}
	
	/**
	 * @param int|null $id 
	 * @return self
	 */
	public function setId(?int $id): self {
		$this->id = $id;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getTitle(): ?string {
		return $this->title;
	}
	
	/**
	 * @param string|null $title 
	 * @return self
	 */
	public function setTitle(?string $title): self {
		$this->title = $title;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getText(): ?string {
		return $this->text;
	}
	
	/**
	 * @param string|null $text 
	 * @return self
	 */
	public function setText(?string $text): self {
		$this->text = $text;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getUser(): ?string {
		return $this->user;
	}
	
	/**
	 * @param string|null $user 
	 * @return self
	 */
	public function setUser(?string $user): self {
		$this->user = $user;
		return $this;
	}

	/**
	 * @return bool|null
	 */
	public function getFavorite(): ?bool {
		return $this->favorite;
	}
	
	/**
	 * @param bool|null $favorite 
	 * @return self
	 */
	public function setFavorite(?bool $favorite): self {
		$this->favorite = $favorite;
		return $this;
	}
	/**
	 */
	public function changeFavorite(){
		$this->favorite = !$this->favorite;
	}

	/**
	 * @return Region|null
	 */
	public function getRegion(): ?Region {
		return $this->region;
	}
	
	/**
	 * @param Region|null $region 
	 * @return self
	 */
	public function setRegion(?Region $region): self {
		$this->region = $region;
		return $this;
	}

	/**
	 * @return Category|null
	 */
	public function getCategory(): ?Category {
		return $this->category;
	}
	
	/**
	 * @param Category|null $category 
	 * @return self
	 */
	public function setCategory(?Category $category): self {
		$this->category = $category;
		return $this;
	}

	/**
	 * @return int|null
	 */
	public function getLikecount(): ?int {
		return $this->likecount;
	}
	
	/**
	 * @param int|null $likecount 
	 * @return self
	 */
	public function setLikecount(?int $likecount): self {
		$this->likecount = $likecount;
		return $this;
	}

	/**
	 * @return DateTime|null
	 */
	public function getDate(): ?DateTime {
		return $this->date;
	}
	
	/**
	 * @param DateTime|null $date 
	 * @return self
	 */
	public function setDate(?DateTime $date): self {
		$this->date = $date;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getImg(): ?string {
		return $this->img;
	}
	
	/**
	 * @param string|null $img 
	 * @return self
	 */
	public function setImg(?string $img): self {
		$this->img = $img;
		return $this;
	}
}
