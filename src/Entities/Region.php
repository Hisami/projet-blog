<?php
namespace App\Entities;

class Region {
    private ?int $id;
    private ?string $name;
	private ?string $rimg;

	private ?string $color;

	private ?string $description;
	

	/**
	 * @param int|null $id
	 * @param string|null $name
	 * @param string|null $rimg
	 * @param string|null $color
	 * @param string|null $description
	 */
	public function __construct(?string $name, ?string $rimg, ?string $color, ?string $description,?int $id) {
		$this->id = $id;
		$this->name = $name;
		$this->rimg = $rimg;
		$this->color = $color;
		$this->description = $description;
	}

	/**
	 * @return int|null
	 */
	public function getId(): ?int {
		return $this->id;
	}
	
	/**
	 * @param int|null $id 
	 * @return self
	 */
	public function setId(?int $id): self {
		$this->id = $id;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getName(): ?string {
		return $this->name;
	}
	
	/**
	 * @param string|null $name 
	 * @return self
	 */
	public function setName(?string $name): self {
		$this->name = $name;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getRimg(): ?string {
		return $this->rimg;
	}
	
	/**
	 * @param string|null $rimg 
	 * @return self
	 */
	public function setRimg(?string $rimg): self {
		$this->rimg = $rimg;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getColor(): ?string {
		return $this->color;
	}
	
	/**
	 * @param string|null $color 
	 * @return self
	 */
	public function setColor(?string $color): self {
		$this->color = $color;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getDescription(): ?string {
		return $this->description;
	}
	
	/**
	 * @param string|null $description 
	 * @return self
	 */
	public function setDescription(?string $description): self {
		$this->description = $description;
		return $this;
	}
}