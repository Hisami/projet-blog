<?php
namespace App\Entities;

class Comment {
    private ?int $id;
	private string $name;
	private string $commenttext;
	private int $articleId;

	/**
	 * @param int|null $id
	 * @param string $name
	 * @param string $commenttext
	 * @param int $articleId
	 */
	public function __construct(string $name, string $commenttext, int $articleId,?int $id) {
		$this->id = $id;
		$this->name = $name;
		$this->commenttext = $commenttext;
		$this->articleId = $articleId;
	}

	/**
	 * @return int|null
	 */
	public function getId(): ?int {
		return $this->id;
	}
	
	/**
	 * @param int|null $id 
	 * @return self
	 */
	public function setId(?int $id): self {
		$this->id = $id;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getName(): string {
		return $this->name;
	}
	
	/**
	 * @param string $name 
	 * @return self
	 */
	public function setName(string $name): self {
		$this->name = $name;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getCommenttext(): string {
		return $this->commenttext;
	}
	
	/**
	 * @param string $commenttext 
	 * @return self
	 */
	public function setCommenttext(string $commenttext): self {
		$this->commenttext = $commenttext;
		return $this;
	}

	/**
	 * @return int
	 */
	public function getArticleId(): int {
		return $this->articleId;
	}
	
	/**
	 * @param int $articleId 
	 * @return self
	 */
	public function setArticleId(int $articleId): self {
		$this->articleId = $articleId;
		return $this;
	}
}