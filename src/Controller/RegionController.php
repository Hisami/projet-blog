<?php

namespace App\Controller;
use App\Entities\Region;
use App\Repository\RegionRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;




#[Route('/api/region')]
class RegionController extends AbstractController {
    private RegionRepository $repo;
    public function __construct(RegionRepository $repo) {
        $this->repo = $repo;
    }

#[Route(methods:'GET')]
    public function all(){
        $regions=$this->repo->findAll();
        return 
        $this->json($regions);
    }

#[Route('/{id}',methods: 'GET')]
    public function findbyid(int $id) {
        $region = $this->repo->findById($id);
        if(!$region){
            throw new NotFoundHttpException();
        }
        return $this->json($region);
    }


#[Route(methods: 'POST')]
    public function add(Request $request, SerializerInterface $serializer) {
        $region = $serializer->deserialize($request->getContent(), Region::class, 'json');
        $this->repo->persist($region);
        return $this->json($region, Response::HTTP_CREATED);
    }
    
#[Route('/{id}', methods:'PUT')]
    public function put(int $id, Request $request, SerializerInterface $serializer) {   
        $region = $this->repo->findById($id);
        if(!$region){
            throw new NotFoundHttpException();
        }
        $toUpdate = $serializer->deserialize($request->getContent(), Region::class,'json');
        $toUpdate->setId($id);
        $this->repo->update($toUpdate);
        return $this->json($toUpdate);
    }


#[Route('/{id}', methods: 'DELETE')]
    public function remove(int $id) {   
    $region = $this->repo->findById($id);
    if(!$region){
        throw new NotFoundHttpException();
    }
    $this->repo->delete($region);
    return $this->json(null, Response::HTTP_NO_CONTENT);
}

}