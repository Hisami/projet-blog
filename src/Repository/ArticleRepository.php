<?php

namespace App\Repository;
use App\Entities\Article;
use App\Entities\Region;
use App\Entities\Category;
use PDO;
use DateTime;


class ArticleRepository {
    private PDO $connection;

    public function __construct() {
        $this->connection = Database::connect();
    } 
/**
     * Méthode qui prend un ligne de résultat PDO et la convertit en instance 
    * de Article
     * @param array $line la ligne de résultat à convertir, sous forme de tableau associatif
     * @return Article l'instance de article
     */

    public function sqlToArticle(array $line):Article {
        return new Article($line['title'],$line['text'],$line['user'],$line['favorite'],$line['likecount'],new DateTime($line['date']),$line['img'],$line['a_id']);
    }
/**
     * Méthode qui prend un ligne de résultat PDO et la convertit en instance 
    * de Region
     * @param array $line la ligne de résultat à convertir, sous forme de tableau associatif
     * @return Region l'instance de region
     */

    private function sqlToRegion(array $line): Region
    {
    return new Region($line["r_n"],$line["rimg"],$line['color'],$line['description'],$line['r_id']);
    }
/**
     * Méthode qui prend un ligne de résultat PDO et la convertit en instance 
    * de Category
     * @param array $line la ligne de résultat à convertir, sous forme de tableau associatif
     * @return Category l'instance de category
     */
    private function sqlToCategory(array $line): Category
    {
    return new Category($line["c_n"],$line['c_id']);
    }    

    /**
     * Summary of findAll
     * @return Article[] les articles de la base de données
     */
    public function findAll():array {
        $articles = [];
        $statement = $this->connection->prepare('SELECT * ,article.id a_id,region.id r_id,region.name r_n,category.id c_id,category.name c_n FROM article LEFT JOIN region ON article.id_region=region.id LEFT JOIN category ON article.id_category=category.id');
        $statement->execute();
        $result=$statement->fetchAll();
        foreach($result as $line) {
            $newarticle = $this->sqlToArticle($line);
            $newarticle->setRegion($this->sqlToRegion($line));
            $newarticle->setCategory($this->sqlToCategory($line));
            $articles[]=$newarticle;
        }
        return $articles;
    }


    /**
     * Summary of persist
     * @param Article $article à faire persister en bdd
     */
    public function persist(Article $article) {
        $statement = $this->connection->prepare('INSERT INTO article (title,text,user,favorite,id_region,id_category,likecount,date,img) VALUES (:title,:text,:user,:favorite,:id_region,:id_category,:likecount,:date,:img)');
        $statement->bindValue('title', $article->getTitle(), PDO::PARAM_STR);
        $statement->bindValue('text', $article->getText(), PDO::PARAM_STR);
        $statement->bindValue('user', $article->getUser(), PDO::PARAM_STR);
        $statement->bindValue('favorite', $article->getFavorite(), PDO::PARAM_BOOL);
        $statement->bindValue('id_region', $article->getRegion()->getId(), PDO::PARAM_INT);
        $statement->bindValue('id_category', $article->getCategory()->getId(), PDO::PARAM_INT);
        $statement->bindValue('likecount', $article->getLikecount(), PDO::PARAM_INT);
        $statement->bindValue('date', $article->getDate()->format('Y-m-d'));
        $statement->bindValue('img', $article->getImg(), PDO::PARAM_STR);
        $statement->execute();

        $article->setId($this->connection->lastInsertId());
    }

    /**
     * Méthode qui effectue une requête SQL vers la base de donnée pour trouve un article spécifique à partir de son id et qui le renvoit comme instance de la classe Article.
     * @param int $id L'id du article à récupérer
     * @return Article l'article récupéré, instance de la classe Article
     */

    public function findById(int $id):?Article {
        $statement = $this->connection->prepare('SELECT * ,article.id a_id,region.id r_id,region.name r_n,category.id c_id,category.name c_n FROM article LEFT JOIN region ON article.id_region=region.id LEFT JOIN category ON article.id_category=category.id WHERE article.id=:id');
        $statement->bindValue('id', $id);
        $statement->execute();
        $result = $statement->fetch();
        if($result) {
            $article=$this->sqlToArticle($result);
            $article->setRegion($this->sqlToRegion($result));
            $article->setCategory($this->sqlToCategory($result));
            return $article;
        }
        return null;
    } 
    /**
     * Permet d'afficher toutes les articles selon son son id_region
     * @param int $id id_region
     * @return array tableau de articles
     */

    public function findByRegionId(int $id):array {
        $articles = [];
        $statement = $this->connection->prepare('SELECT * ,article.id a_id,region.id r_id,region.name r_n,category.id c_id,category.name c_n FROM article LEFT JOIN region ON article.id_region=region.id LEFT JOIN category ON article.id_category=category.id WHERE id_region=:id');
        $statement->bindValue('id', $id);
        $statement->execute();
        $results = $statement->fetchAll();
        foreach($results as $line) {
            $article=$this->sqlToArticle($line);
            $article->setRegion($this->sqlToRegion($line));
            $article->setCategory($this->sqlToCategory($line));
            $articles[]=$article;
        }
        return $articles;
    }
/**
     * Permet d'afficher toutes les articles selon son son id_category
     * @param int $id id_category
     * @return array tableau de articles
     */

    public function findByCategoryId(int $id):array {
        $articles = [];
        $statement = $this->connection->prepare('SELECT * ,article.id a_id,region.id r_id,region.name r_n,category.id c_id,category.name c_n FROM article LEFT JOIN region ON article.id_region=region.id LEFT JOIN category ON article.id_category=category.id WHERE id_category=:id');
        $statement->bindValue('id', $id);
        $statement->execute();
        $results = $statement->fetchAll();
        foreach($results as $line) {
            $article=$this->sqlToArticle($line);
            $article->setRegion($this->sqlToRegion($line));
            $article->setCategory($this->sqlToCategory($line));
            $articles[]=$article;
        }
        return $articles;
    }
/**
     * Permet d'afficher toutes les articles dont son favorite est true
     * @return array tableau de articles
     */
    public function findAllFavorite():array {
        $articles = [];
        $statement = $this->connection->prepare('SELECT * ,article.id a_id,region.id r_id,region.name r_n,category.id c_id,category.name c_n FROM article LEFT JOIN region ON article.id_region=region.id LEFT JOIN category ON article.id_category=category.id WHERE favorite=1');
        $statement->execute();
        $results = $statement->fetchAll();
        foreach($results as $line) {
            $article=$this->sqlToArticle($line);
            $article->setRegion($this->sqlToRegion($line));
            $article->setCategory($this->sqlToCategory($line));
            $articles[]=$article;
        }
        return $articles;
    }
/**
     * Méthode qui permet de supprimer un article de la base de donnée à partir de son instance Article
     * @param Article $article, l'article à supprimer
     * @return $result
     */

    public function delete(Article $article){
        $statement = $this->connection->prepare('DELETE FROM article WHERE article.id = :id');
        $statement->bindValue('id', $article->getId(), PDO::PARAM_INT);
        $statement->execute();
    }
    
/**
     * Permet de modifier un article dans la base de donnée en lui fournissant une nouvelle instance de Article avec le bon id
     * @param Article $article le nouvel article que l'on veut
     * @return void
     */

    public function update(Article $article) {
        $statement = $this->connection->prepare('UPDATE article SET title=:title,text=:text,user=:user,id_region=:id_region,id_category=:id_category,date=:date,img=:img WHERE id=:id');
        $statement->bindValue('title', $article->getTitle(),PDO::PARAM_STR);
        $statement->bindValue('text', $article->getText(), PDO::PARAM_STR);
        $statement->bindValue('user', $article->getTitle(),PDO::PARAM_STR);
        $statement->bindValue('id_region', $article->getRegion()->getId(), PDO::PARAM_INT);
        $statement->bindValue('id_category', $article->getCategory()->getId(), PDO::PARAM_INT);
        $statement->bindValue('date', $article->getDate()->format('Y-m-d'));
        $statement->bindValue('img', $article->getImg(), PDO::PARAM_STR);
        $statement->bindValue('id', $article->getId(), PDO::PARAM_INT);
        $statement->execute();
    }
    /**
     * Permet de modifier "favorite"  d'un article dans la base de donnée en lui fournissant une nouvelle instance de Article avec le bon id
     * @param Article $article le nouvel article que l'on veut
     * @return void
     */
    public function updateFavorite(Article $article) {
        $statement = $this->connection->prepare('UPDATE article SET favorite=:favorite WHERE id=:id');
        $statement->bindValue('favorite', $article->getFavorite(), PDO::PARAM_BOOL);
        $statement->bindValue('id', $article->getId(), PDO::PARAM_INT);
        $statement->execute();
    }
    /**
     * Permet de modifier "likecount" d'un article dans la base de donnée en lui fournissant une nouvelle instance de Article avec le bon id
     * @param Article $article le nouvel article que l'on veut
     * @return void
     */
    public function updateLikecount(Article $article) {
        $statement = $this->connection->prepare('UPDATE article SET likecount=:likecount WHERE id=:id');
        $statement->bindValue('likecount', $article->getLikecount(), PDO::PARAM_INT);
        $statement->bindValue('id', $article->getId(), PDO::PARAM_INT);
        $statement->execute();
    }

}