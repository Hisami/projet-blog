<?php

namespace App\Repository;
use App\Entities\Comment;
use PDO;


class CommentRepository {
    private PDO $connection;

    public function __construct() {
        $this->connection = Database::connect();
    } 
    /**
     * Summary of findAllbyId
     * @return Comment[] les comments de la base de données selon id Article
     */
    public function findAllbyId(int $id):array {
        $comments = [];
        $statement = $this->connection->prepare('SELECT * FROM comment WHERE id_article=:id');
        $statement->bindValue('id', $id, PDO::PARAM_INT);
        $statement->execute();
        $result=$statement->fetchAll();
        foreach($result as $line) {
            $comments[] = new Comment($line['cuser'],$line['ctext'],$line['id_article'],$line['id']);
        }
        return $comments;
    }


    /**
     * Summary of persist
     * @param Comment $comment le commentaire à faire persister en bdd
     */
    public function persist(Comment $comment) {
        $statement = $this->connection->prepare('INSERT INTO comment (cuser,ctext,id_article) VALUES (:cuser,:ctext,:id_article)');
        $statement->bindValue('cuser', $comment->getName(), PDO::PARAM_STR);
        $statement->bindValue('ctext', $comment->getCommenttext(), PDO::PARAM_STR);
        $statement->bindValue('id_article', $comment->getArticleId(), PDO::PARAM_INT);
        $statement->execute();
        $comment->setId($this->connection->lastInsertId());
    }


    public function delete(Comment $comment){
        $statement = $this->connection->prepare('DELETE FROM comment WHERE id = :id');
        $statement->bindValue('id', $comment->getId(), PDO::PARAM_INT);
        $statement->execute();
    }

}
