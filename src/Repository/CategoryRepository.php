<?php

namespace App\Repository;
use App\Entities\Category;
use PDO;


class CategoryRepository {
    private PDO $connection;

    public function __construct() {
        $this->connection = Database::connect();
    } 
    /**
     * Summary of findAll
     * @return Category[] les categories de la base de données
     */
    public function findAll():array {
        $categories = [];
        $statement = $this->connection->prepare('SELECT * FROM category');
        $statement->execute();
        $result=$statement->fetchAll();
        foreach($result as $line) {
            $categories[] = new Category($line['name'],$line['id']);
        }
        return $categories;
    }


    /**
     * Summary of persist
     * @param Category $category le category à faire persister en bdd
     */
    public function persist(Category $category) {

        $statement = $this->connection->prepare('INSERT INTO category (name) VALUES (:name)');

        $statement->execute([
            'name' => $category->getName(),
        ]);

        $category->setId($this->connection->lastInsertId());
    }

    public function findById(int $id):?Category {
        $statement = $this->connection->prepare('SELECT * FROM category WHERE id=:id');
        $statement->bindValue('id', $id);
        $statement->execute();
        $result = $statement->fetch();
        if($result) {
            return new Category($result['name'],$result['id']);
        }
        return null;
    }
    public function delete(Category $category){
        $statement = $this->connection->prepare('DELETE FROM category WHERE id = :id');
        $statement->bindValue('id', $category->getId(), PDO::PARAM_INT);
        $statement->execute();
    }

    public function update(Category $category) {
        $statement = $this->connection->prepare('UPDATE category SET name=:name WHERE id=:id');
        $statement->bindValue('name', $category->getName());
        $statement->bindValue('id', $category->getId(), PDO::PARAM_INT);
        $statement->execute();
    }

}
