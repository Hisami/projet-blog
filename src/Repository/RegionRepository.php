<?php

namespace App\Repository;
use App\Entities\Region;
use PDO;


class RegionRepository {
    private PDO $connection;

    public function __construct() {
        $this->connection = Database::connect();
    } 


    public function sqlToRegion(array $line): Region
    {
    return new Region($line["name"],$line["rimg"],$line['color'],$line['description'],$line["id"]);
    }

    /**
     * Summary of findAll
     * @return Region[] les regions de la base de données
     */
    public function findAll():array {
        $regions = [];
        $statement = $this->connection->prepare('SELECT * FROM region');
        $statement->execute();
        $result=$statement->fetchAll();
        foreach($result as $line) {
            $regions[] = $this->sqlToRegion($line);
        }
        return $regions;
    }


    /**
     * Summary of persist
     * @param Region $region la region à faire persister en bdd
     */
    public function persist(Region $region) {

        $statement = $this->connection->prepare('INSERT INTO region (name,rimg) VALUES (:name,:rimg)');

        $statement->execute([
            'name' => $region->getName(),
            'rimg'=>$region->getRimg()
        ]);

        $region->setId($this->connection->lastInsertId());
    }

    public function findById(int $id):?Region {
        $statement = $this->connection->prepare('SELECT * FROM region WHERE id=:id');
        $statement->bindValue('id', $id);
        $statement->execute();
        $result = $statement->fetch();
        if($result) {
            return $this->sqlToRegion($result);
        }
        return null;
    }
    public function delete(Region $region){
        $statement = $this->connection->prepare('DELETE FROM region WHERE id = :id');
        $statement->bindValue('id', $region->getId(), PDO::PARAM_INT);
        $statement->execute();
    }

    public function update(Region $region) {
        $statement = $this->connection->prepare('UPDATE region SET name=:name WHERE id=:id');
        $statement->bindValue('name', $region->getName());
        $statement->bindValue('rimg', $region->getRimg());
        $statement->bindValue('id', $region->getId(), PDO::PARAM_INT);
        $statement->execute();
    }

}
